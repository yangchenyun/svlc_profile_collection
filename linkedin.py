import os
from concurrent import futures
import json

import click
from linkedin_api import Linkedin

USER_EMAIL= os.environ['LINKEDIN_EMAIL']
USER_PASS= os.environ['LINKEDIN_PASS']

api = Linkedin(USER_EMAIL, USER_PASS)

@click.command()
@click.argument('linkedin_ids', nargs=-1)
def print_profiles(linkedin_ids):
    with futures.ThreadPoolExecutor(max_workers=10) as executor:
        profiles = executor.map(api.get_profile, linkedin_ids)
    click.echo(json.dumps(list(profiles)))


if __name__ == '__main__':
    print_profiles()
