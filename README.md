# Profile Collector

## Prerequisite

python >= 3.6

jq

## Installation

``` bash
pip3 install -r requirements.txt
```

## Usage

Modify the `.env` file and type in Linkedin credentials.


``` bash
python3 linkedin.py <linked_id>...

# For Example
python3 linkedin.py yangchenyun ymchen | jq .

# To save to a file
python3 linkedin.py yangchenyun ymchen | jq . > sample.json
```

## TODO

Query would be performed with the query language `jq`.
